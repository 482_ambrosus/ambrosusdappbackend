const mongoose = require('mongoose');
module.exports = mongoose.Schema({
  statementId: { type: String },
  tradeId: { type: Number, required: true },
  data: { type: String, required: true },
  dt: { type: Date, required: true, default: Date.now },
});
