const config = require('config');
const provider = config.get('provider');
const errors = require('../errors');
const mongoose = require('mongoose');
const StatementSchema = require('../Schemas/Statement');
const Statement = mongoose.model('Statement', StatementSchema);
const Web3 = require('web3');
let web3 = new Web3(new Web3.providers.HttpProvider(provider));
let utils = require('ethereumjs-util');
const tradesAbi = require('../abi/Trades');
const tradesContractAddr = config.get('tradesContractAddr');
const s3 = require('../lib/s3');

// StatementSchema.pre('save', function (next) {
//   this.statementId = web3.utils.sha3(String(this.get('_id')));
//   next();
// });

function isSha3(a) {
  return /0x[0-9a-f]{64}/i.test(a);
}
function isSig(a) {
  return /0x[0-9a-f]{130}/i.test(a);
}

function addressFromSignature(msg,signature) {
  if(!isSha3(msg)){
    throw new Error('incorrectMessageToSigCheck');
  }
  if(!isSig(signature)){
    throw new Error('incorrectSignature');
  }
  let r = utils.toBuffer(signature.slice(0,66));
  let s = utils.toBuffer('0x' + signature.slice(66,130));
  let v = utils.bufferToInt(utils.toBuffer('0x' + signature.slice(130,132)));
  let m = utils.toBuffer(msg);
  let pub = utils.ecrecover(m, v, r, s);
  let addr = '0x' + utils.pubToAddress(pub).toString('hex');
  return addr;
}
async function getTradePermission(addr,tradeId) {
  let tradesContract = new web3.eth.Contract(tradesAbi,tradesContractAddr);
  let permissions;
  try {
    permissions = await web3.eth.call({
      to: tradesContractAddr,
      data: tradesContract.methods.getPermission(tradeId, addr).encodeABI()
    });
  } catch (e) {
    console.error(e);
    return errors.blockchainError;
  }
  return web3.utils.hexToNumber(permissions);
}
async function findStatementInContract(tradeId, statementId, addr) {
  let tradesContract = new web3.eth.Contract(tradesAbi,tradesContractAddr);
  try {
    let statements = await tradesContract.getPastEvents('Statement', {
      fromBlock: 0,
      toBlock: 'latest',
      filter: {
        statementId,
        tradeId,
        from: addr
      }
    });
    return statements.length !== 0;
  } catch (e) {
    console.error(e);
    // return errors.blockchainError;
  }
  return false;

}
async function addStatementDocument(data, tradeId, statementId) {
  let statement = await (new Statement({
    data,
    tradeId,
    statementId
  })).save();
  return statement.statementId;
}
async function getStatements(tradeId, signature) {
  let addr;
  try {
    addr = addressFromSignature(web3.utils.sha3(tradeId), signature);
  } catch (e) {
    if(errors[e.message]){
      return errors[e.message];
    }
    else{
      return errors.unexpected;
    }
  }
  let permissions = await getTradePermission(addr, tradeId);
  if(permissions === 0) {
    return errors.wrongContractPermission;
  }

  try {
    return await Statement.find({tradeId: tradeId},{_id: 0, __v:0});
  } catch (e) {
    return errors.dbFailure;
  }
}

async function addStatement(params) {
  if(!(params.signature && params.tradeId && params.statement && params.statementId)){
    return errors.wrongParams;
  }
  let addr;
  try {
    addr = addressFromSignature(web3.utils.sha3(params.tradeId), params.signature);
  } catch (e) {
    if(errors[e.message]){
      return errors[e.message];
    }
    else{
      return errors.unexpected;
    }
  }
  let permissions = await getTradePermission(addr, params.tradeId);
  if(permissions !== 2) {
    return errors.wrongContractPermission;
  }
  try {
    if(findStatementInContract(params.tradeId, params.statementId, addr)){
      await addStatementDocument(params.statement, params.tradeId, params.statementId);
      return {success:true};
    }
    return errors.statementNotFound;
  } catch (e) {
    return errors.dbFailure;
  }
}
async function addStatementFile(params) {
  if(!(params.signature && params.statementId && params.file &&  params.contentType)){
    return errors.wrongParams;
  }
  await s3.putFile('ambrosus-statements', params.statementId, params.file, params.contentType);
  return {success: true};
}
async function addAssetFile(params) {
  if(!(params.signature && params.assetId && params.file &&  params.contentType)){
    return errors.wrongParams;
  }
  await s3.putFile('ambrosus-assets', params.assetId, params.file, params.contentType);
  return {success: true};
}
module.exports = {
  getStatements,
  addStatement,
  addAssetFile,
  addStatementFile
};
