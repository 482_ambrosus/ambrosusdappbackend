const AWS = require('aws-sdk');
AWS.config.loadFromPath('./config/default.json');
const S3 = new AWS.S3();

async function putFile(bucket, name, base64body, contentType) {
  await S3.putObject({
    Bucket: bucket,
    Key: name,
    Body: new Buffer(base64body, 'base64'),
    ContentType: contentType
  }).promise();
}

async function getFile(bucket, name) {
  let Obj = await S3.getObject({ Bucket: bucket, Key: name}).promise();
  return {
    body: Obj.Body,
    contentType: Obj.ContentType
  };
}

// putFile('ambrosus-statements','test.txt',new Buffer('h31100').toString('base64')).then(()=>getFile('ambrosus-statements','test.txt')).then(console.log);
module.exports = {
  putFile,
  getFile
};
