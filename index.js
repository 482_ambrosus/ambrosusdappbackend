const config = require('config');
const db = config.get('db');
const host = config.get('host');
const port = config.get('port');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const express = require('express');
const app = express();
let bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '150mb'}));
const errors = require('./errors');
const main = require('./lib/main');
const s3 = require('./lib/s3');

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  res.header('Access-Control-Allow-Origin', '*');

  //intercepts OPTIONS method
  if ('OPTIONS' === req.method) {
    //respond with 200
    res.send(200);
  }
  else {
  //move on
    next();
  }
});

app.get('/statements/:tradeId/:signature', function (req, res) {
  main.getStatements(req.params.tradeId, req.params.signature).then((x)=>res.json(x)).catch((e)=>{
    console.error(e);
    res.json(errors.unexpected);
  });
});
app.post('/statement', function (req, res) {
  main.addStatement(req.body).then((x)=>res.json(x)).catch((e)=>{
    console.error(e);
    res.json(errors.unexpected);
  });
});
app.get('/files/statement/:statementId', function (req, res) {
  s3.getFile('ambrosus-statements',req.params.statementId).then((x) => {
    res.set('Content-Type', x.contentType);
    res.send(x.body);
  }).catch((e)=>{
    console.error(e);
    res.json(e);
  });
});
app.post('/files/statement', function (req, res) {
  main.addStatementFile(req.body).then((x)=>res.json(x)).catch((e)=>{
    console.error(e);
    res.json(errors.unexpected);
  });
});
app.get('/files/asset/:assetId', function (req, res) {
  s3.getFile('ambrosus-assets',req.params.assetId).then((x) => {
    res.set('Content-Type', x.contentType);
    res.send(x.body);
  }).catch((e)=>{
    console.error(e);
    res.json(errors.s3Error);
  });
});
app.post('/files/asset', function (req, res) {
  main.addAssetFile(req.body).then((x)=>res.json(x)).catch((e)=>{
    console.error(e);
    res.json(errors.unexpected);
  });
});
app.all('*', (req, res) => res.json({}));
mongoose.connect(db).then(()=>app.listen(port,host)).catch(()=>console.error(errors.dbFailure));
