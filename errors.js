module.exports = {
  incorrectSignature: {
    code: 1,
    data: 'incorrect singature'
  },
  invalidSignature: {
    code: 2,
    data: 'invalid singature'
  },
  incorrectMessageToSigCheck: {
    code: 3,
    data: 'incorrect message'
  },
  wrongParams: {
    code: 4,
    data: 'wrong params'
  },
  blockchainError: {
    code: 5,
    data: 'blockchain error'
  },
  wrongContractPermission: {
    code: 6,
    data: 'user has not permissions'
  },
  dbFailure: {
    code: 7,
    data: 'database failure'
  },
  statementNotFound: {
    code: 8,
    data: 'statement is not found'
  },
  s3Error: {
    code: 9,
    data: 'S3 error'
  },
  unexpected: {
    code: 99,
    data: 'unexpected error'
  },
};
