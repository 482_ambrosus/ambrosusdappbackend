# Env #
**https://amb.482.solutions**


# Deploy #
```sh
npm install
npm start
```

# Config #
*./config/default.json*

# Error codes #
*./errors.js*


# API reference #

## GET /statements/:tradeId/:signature ##


Getting of statements.
Returns content of statements sequence

*tradeId* - id of the trade i.e. '*1*'

*signature* - ECDSA signature of tradeId i.e. '*0xb3062c870ba4b512f55b3927237f171011e6527656672d43dba9cff56ebd9a4e561de52cb2c6544d3aa41ea61fba9331ddec03f63e151913e6a160b9cff83cfb1b*'

**Response**
```json
[{
    "dt": "2018-01-11T18:35:54.008Z",
    "data": "Hello world!",
    "tradeId": 0,
    "statementId": "0x16876d429d9c9d038e16a2b2104c7e771a7ed2e22a69b66fac2d192af95ab39e"
}, {
    "dt": "2018-01-11T18:48:18.975Z",
    "data": "Hello world2!",
    "tradeId": 0,
    "statementId": "0x39543f828e4e52fd00ba389742c66ce94b5cdbac11c63887de3aa4b89ad4df7b"
}]
```

## POST /statement ##


Adding a statement.
Returns statementId of the new statement.

**Request body**

```json
{
    "tradeId": "0",
    "statement": "Hello world!",
    "signature": "0xb3062c870ba4b512f55b3927237f171011e6527656672d43dba9cff56ebd9a4e561de52cb2c6544d3aa41ea61fba9331ddec03f63e151913e6a160b9cff83cfb1b"
}
```

**Response**
```json
[{
    "dt": "2018-01-11T18:35:54.008Z",
    "data": "Hello world!",
    "tradeId": 0,
    "statementId": "0x16876d429d9c9d038e16a2b2104c7e771a7ed2e22a69b66fac2d192af95ab39e"
}, {
    "dt": "2018-01-11T18:48:18.975Z",
    "data": "Hello world2!",
    "tradeId": 0,
    "statementId": "0x39543f828e4e52fd00ba389742c66ce94b5cdbac11c63887de3aa4b89ad4df7b"
}]
```
