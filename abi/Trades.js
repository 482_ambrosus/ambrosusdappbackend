module.exports = [
  {
    'constant': true,
    'inputs': [
      {
        'name': 'tradeId',
        'type': 'uint256'
      },
      {
        'name': 'addr',
        'type': 'address'
      }
    ],
    'name': 'getPermission',
    'outputs': [
      {
        'name': '',
        'type': 'uint8'
      }
    ],
    'payable': false,
    'stateMutability': 'view',
    'type': 'function'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': 'tradeId',
        'type': 'uint256'
      }
    ],
    'name': 'finishTrade',
    'outputs': [],
    'payable': false,
    'stateMutability': 'nonpayable',
    'type': 'function'
  },
  {
    'anonymous': false,
    'inputs': [
      {
        'indexed': false,
        'name': 'tradeId',
        'type': 'uint256'
      },
      {
        'indexed': false,
        'name': 'from',
        'type': 'address'
      },
      {
        'indexed': false,
        'name': 'statementId',
        'type': 'uint256'
      },
      {
        'indexed': false,
        'name': 'fileId',
        'type': 'uint256'
      }
    ],
    'name': 'File',
    'type': 'event'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': 'tradeId',
        'type': 'uint256'
      },
      {
        'name': 'participantAddr',
        'type': 'address'
      },
      {
        'name': 'p',
        'type': 'uint8'
      }
    ],
    'name': 'setPermission',
    'outputs': [],
    'payable': false,
    'stateMutability': 'nonpayable',
    'type': 'function'
  },
  {
    'anonymous': false,
    'inputs': [
      {
        'indexed': false,
        'name': 'tradeId',
        'type': 'uint256'
      },
      {
        'indexed': false,
        'name': 'owner',
        'type': 'address'
      },
      {
        'indexed': false,
        'name': 'p',
        'type': 'uint8'
      }
    ],
    'name': 'NewTrade',
    'type': 'event'
  },
  {
    'anonymous': false,
    'inputs': [
      {
        'indexed': false,
        'name': 'tradeId',
        'type': 'uint256'
      },
      {
        'indexed': false,
        'name': 'from',
        'type': 'address'
      },
      {
        'indexed': false,
        'name': 'statementId',
        'type': 'uint256'
      }
    ],
    'name': 'Statement',
    'type': 'event'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': 'tradeId',
        'type': 'uint256'
      },
      {
        'name': 'statementId',
        'type': 'uint256'
      },
      {
        'name': 'fileId',
        'type': 'uint256'
      }
    ],
    'name': 'addStatement',
    'outputs': [],
    'payable': false,
    'stateMutability': 'nonpayable',
    'type': 'function'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': 'tradeId',
        'type': 'uint256'
      },
      {
        'name': 'participantAddr',
        'type': 'address'
      }
    ],
    'name': 'deleteParticipants',
    'outputs': [],
    'payable': false,
    'stateMutability': 'nonpayable',
    'type': 'function'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': 'assetId',
        'type': 'string'
      }
    ],
    'name': 'makeTrade',
    'outputs': [],
    'payable': false,
    'stateMutability': 'nonpayable',
    'type': 'function'
  },
  {
    'anonymous': false,
    'inputs': [
      {
        'indexed': false,
        'name': 'tradeId',
        'type': 'uint256'
      },
      {
        'indexed': false,
        'name': 'linkedTradeId',
        'type': 'uint256'
      }
    ],
    'name': 'linkedTrade',
    'type': 'event'
  },
  {
    'constant': false,
    'inputs': [
      {
        'name': 'tradeId',
        'type': 'uint256'
      },
      {
        'name': 'linkedTradeId',
        'type': 'uint256'
      }
    ],
    'name': 'linkTrade',
    'outputs': [],
    'payable': false,
    'stateMutability': 'nonpayable',
    'type': 'function'
  }
];
